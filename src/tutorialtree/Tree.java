package tutorialtree;

/**
 *
 * @author Natalia
 * @author Carlos Mestre 1335543 cmest005@fiu.edu
 * @author Josue Bohorquez
 * @author Gersain Mesa
 * @author Steven Gculis
 * @author Nilton Munoz
*/
/*
Create a tree
*/
public class Tree
{
    private TreeNode root ; 
    private int count; // number of nodes in the Binary Search Tree
    
    
    /*
    Create an empty tree with no root
    */
    public Tree()
    {
        root = null ; 
        count = 0;
    }        
    
    /*
    Create a tree with a selected root 
    */
    public Tree (int newRoot)
    {
        root = new TreeNode(newRoot, null, null) ;
        count = 0;
    }
    
    /*
     * Returns root
     */
    public TreeNode getRoot()
    {
        return root;
    }
    
    /*
    Erase the nodes from the tree
    */
    public void eraseTree()
    {
        root = null ; 
        count = 0;
    }
    
    /*
     * Test if there are no nodes in the tree
    */
    public boolean isEmpty() 
    {
        if (root == null){
            return true;
        }
        else{
            return false ;
        } 
    } 
    
    /*
    Total amount of nodes
    */
    public int size()
    {
        //return TreeNode.size(root) ; 
        return count;
    }    
    
    
    
    //natali
    public void insert (int d)
    {
        root = helperInsert(root, d);
        count ++;
    }
    
    // Helper method to insert a node in a subtree (allowing duplicates)
    private TreeNode helperInsert (TreeNode root, int d)
    {
        if (root == null)
        {
            return new TreeNode (d, null, null);
        }
        if (root.getData() >= d)
        {
            root.setLeft(helperInsert(root.getLeft(), d));
        }
        else
        {
            root.setRight(helperInsert(root.getRight(),d));
        }
        return root;
    }
    
    //Natalia
    public boolean contains (int d)
    {
        return helperContains(root, d);
    }
    
    //Helper method to check Check if the specified value is contained in the tree
    private boolean helperContains (TreeNode root, int d)
    {
        if (root == null)
        {
            return false;
        }
        if (root.getData() == d)
        {
            return true;
        }
        else if (root.getData() > d)
        {
            return helperContains(root.getLeft(), d);
        }
        else
        {
            return helperContains(root.getRight(),d);
        }
                    
    }
    
    //Natalia
    public void remove (int d)
    {
        boolean found = contains(d);
        if (found)
        {
            root = helperRemove(root, d);
            count--;
        }
    }
    
    //Helper method to remove a node with a specified value
    private TreeNode helperRemove (TreeNode root, int d)
    {
        if (root == null)
        {
            return root;
        }
        if (root.getData() > d)
        {
            root.setLeft(helperRemove(root.getLeft(), d));
        }
        else if (root.getData() < d)
        {
            root.setRight(helperRemove(root.getRight(), d));
        }
        else // found the specified value in the root
        {
            if (root.getLeft() == null)
            {
                return root.getRight();
            }
            else if (root.getRight() == null)
            {
                return root.getLeft();
            }
            else   // root has 2 children
            {
                TreeNode leftMax = helperFindMaxNode(root.getLeft());
                root.setData(leftMax.getData());
                TreeNode newLeftChild = removeMax(root.getLeft());
                root.setLeft(newLeftChild );
            }
        }
        
        return root;
        
    }
    
    //Natalia
    // Remove node with a maximum value in a subtree
    // Return pointer to a subtree with maximum value removed
    private TreeNode removeMax(TreeNode rt)
    {
        if(rt.getRight() == null)
        {
            return rt.getLeft();
        }
        rt.setRight(removeMax(rt.getRight()));
        return rt;
    }
    
    //natalia
    public int findMax()
    {
        // Tree is empty, nothing to return
        if (root == null)
        {
            return -1111111;
        }  
        return findMaxNode().getData();
    }
    
    //natalia
    //Find and return node with maximum value in the tree
    public TreeNode findMaxNode()
    {
        return helperFindMaxNode(root);
    }
    
    //Helper method to find and return node with maximum value in a subtree
    private TreeNode helperFindMaxNode(TreeNode rt)
    {
        if (rt == null)
        {
            return null;
        } 
        if (rt.getRight() == null)
        {
            return rt;
        }
        return helperFindMaxNode(rt.getRight());
    }
    
    //Natalia
    public int findMin()
    {
        if (root == null)
        {
            return -1111111;
        }  // Tree is empty, nothing to return
        return findMinNode().getData();
    }
    
    //Find and return node with minimum value in the tree
    public TreeNode findMinNode()
    {
        return helperFindMinNode(root);
    }
    
    //Helper method to find and return node with minimum value in a subtree
    private TreeNode helperFindMinNode(TreeNode rt)
    {
        if (rt == null)
        {
            return null;
        }  
        if (rt.getLeft() == null)
        {
            return rt;
        }
        return helperFindMinNode(rt.getLeft());
    }
    
     /*
      *Height of tree
    */
    public int height(TreeNode node) {
        int height = 0;

        if (node == null) {
            height = -1;
        } 
        else {
            height = 1 + Math.max(height(node.getLeft()), height(node.getRight()));
        }
        return height;
    }
    
    // Prints subtrees of the overall tree
    public static void printSubtree(TreeNode root) {
	// implicit base case: if null, do nothing
	if (root != null) {
            printSubtree(root.getLeft());					// print my left sub-tree
            System.out.print(root.getData() + " ");	// print it out
            printSubtree(root.getRight());					// print my right sub-tree
	}
    }
    
    /*
     * Returns the level of a given node    
     * Carlos Mestre
    */    
    public int nodeLevel(int data)
    {
        return nodeLevel(root, data, 1) ; 
    } 
    
    /*
    Helper for nodeLevel
    */
    private int nodeLevel(TreeNode root, int data, int level)
    {         
        if(root == null)
        {
            throw new NullPointerException() ;
        } 
        if(root.getData() == data)
        {
            return level ;
        } 
        if(root.getData() >= data)
        {
            level++ ; 
            return nodeLevel(root.getLeft(), data, level ) ; 
        }
        if(root.getData() <= data)
        {
            level++ ; 
            return nodeLevel(root.getRight(), data, level) ; 
        }        
        return level ;                     
    }
    
    
    /*
     * Displays the path from root to given node 
     * Carlos Mestre
    */
    public String pathToNode(int data)
    {
        String path = ""  ;           
        return "Path to node is: " + pathToNode(root , data, path) ; 
    }
    /*
    private helper method
    */
    private String pathToNode(TreeNode root, int data, String path)
    {       
        if(root == null)
        {
            throw new NullPointerException() ;
        }
        if(root.getData() == data)
        { 
            path = path + Integer.toString(root.getData())   ;
        } 
        else if(root.getData() >= data)
        {
            path = path + Integer.toString(root.getData()) + " --> "  ;
            return pathToNode(root.getLeft(), data, path) ; 
        }
        else if(root.getData() <= data)
        {
            path = path + Integer.toString(root.getData()) + " --> "  ;
            return pathToNode(root.getRight(), data, path) ; 
        }    
        return path ;        
    }
    
    
    /*
     * Dtermine leaf nodes in a tree.
     * Josue Bohorquez.
     */
    public String leafNodes(){
        String leafs = "";
        
        
        return "Leafs found: " + leafNodes(root, leafs);
    }
    
    private String leafNodes(TreeNode root, String leafs){
      
      if (root == null){
          throw new NullPointerException();
      }
      if (root.isLeaf()){
          return Integer.toString(root.getData());
      } 
      
      else{
          if(root.getLeft() != null){
             leafs += leafNodes(root.getLeft(), leafs) + " ";
          }
          if (root.getRight() != null){
              leafs += leafNodes(root.getRight(), leafs);
          }
      }
       return leafs ;        
        
        
    }
    
    //Natalia
    public void children (TreeNode rt)
            {
                if (rt == null)
                {
                    System.out.println("The node is null and it has no children.");
                }
                else if (rt.getLeft() != null && rt.getRight() != null) 
                {
                    System.out.println("The parent is " + rt.getData() + ". It has 2 non-empty children. The left child of the node is " + rt.getLeft().getData() 
                                       + " and the right child of the node is " + rt.getRight().getData());
                }
                else if (rt.getLeft() != null)
                {
                    System.out.println("The parent is " + rt.getData() + ". It has 2 children. The left child of the node is " + rt.getLeft().getData()
                                       + " and the right child is empty.");
                }
                else if (rt.getRight() != null)
                {
                    System.out.println("The parent is " + rt.getData() + ". It has 2 children. The right child of the node is " + rt.getRight().getData()
                                       + " and the left child is empty.");
                }
                else
                {
                    System.out.println("The parent is " + rt.getData() + ". It has 2 empty children.");
                }
            }
}
