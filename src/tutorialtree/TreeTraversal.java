package tutorialtree;


/**
 *
 * @author Natalia
 * @author Josue Bohorquez
 * @author Gersain Mesa
 */
public class TreeTraversal {
    
   /*
    * Natalia Filippova
    */
    public static void inorder (Tree tree)
   {
       helperInorder(tree.getRoot());
   }
    private static void helperInorder(TreeNode rt)
   {
       if (rt == null)
       {
           return;
       }
       helperInorder(rt.getLeft());
       System.out.println(rt.getData());
       helperInorder(rt.getRight());
   }
   
   /*
    * Josue Bohorquez
    * Traverse the tree in Preorder
    * Middle, Left, Right.
    */
   public static void preOrder(Tree tree){
       helperPreOrder(tree.getRoot());
   }
   private static void helperPreOrder(TreeNode root)
    {
        if (root == null){
            return ;
        }
        System.out.println(root.getData());
        helperPreOrder(root.getLeft());
        helperPreOrder(root.getRight());
    }

    
   /*
    * Gersain Mesa
    */
   public static void postOrder (Tree tree)
   {
       helperPostOrder(tree.getRoot());
   }
   private static void helperPostOrder(TreeNode root)
   {
       
      if (root == null)
      {
          return;
      }
      helperPostOrder(root.getLeft());
      helperPostOrder(root.getRight());
      System.out.println(root.getData());
              
      
   }

}