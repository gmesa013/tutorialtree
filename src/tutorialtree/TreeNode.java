package tutorialtree;

/**
 *
 * @author Natalia
 * @author Carlos Mestre 1335543 cmest005@fiu.edu
*/


/*
Create either an empty TreeNode or a TreeNode with passed parameters
*/
public class TreeNode
{
    private int data ;
    private TreeNode left ; 
    private TreeNode right ; 
    
    /*
    Constructor for an empty TreeNode
    */
    public TreeNode()
    {
        this(0,null,null) ; 
    }        
    
    /*
    Constructor for a TreeNode with 3 passed parameters
    */
    public TreeNode(int d, TreeNode l, TreeNode r)
    {
        data = d ;
        left = l ; 
        right = r ; 
    }
    
    /*
    Retrieve the TreeNode's data
    */
    public int getData()
    {
        return data ; 
    }        
    
    /*
    Set the TreeNode's data
    */
    public void setData(int d)
    {
        data = d ; 
    }        
    
    /*
    Retrieve the pointer to the next left Node
    */
    public TreeNode getLeft() 
    {
        return left ; 
    }        
    
    /*
    Set the pointer to the next left Node
    */
    public void setLeft(TreeNode node)
    {
        left = node ; 
    }
    
    /*
    Retrieve the pointer to the next right Node
    */
    public TreeNode getRight()
    {
        return right ; 
    }   
    /*
    Set the pointer to the next right Node
    */
    public void setRight(TreeNode node)
    {
        right = node ; 
    } 
    
    /*
     * determine if this node has children if it doesnt is a leaf
     * Josue Bohorquez
     */
    public boolean isLeaf(){
        return (this.left == null && this.right == null);
    }
}
   
