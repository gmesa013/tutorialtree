package tutorialtree;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Scanner;

/**
 *
 * @author Natalia
 */
public class BSTtutorial {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        // I just tested some methods here
        Tree tr = new Tree();
        System.out.println("Enter 17 integers to create a binary search tree");//for example user input: 45 67 23 44 32 67 55 88 23 12 68 9 50 2 4 87 5
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 3; i++)
        {
            tr.insert(sc.nextInt());
        }
        System.out.println("\n");
        TreeTraversal.inorder(tr);
        System.out.println("\n");
        
        System.out.println(tr.leafNodes());
//        System.out.println("Maximum value is " + tr.findMax());
//        System.out.println("Minimum value is " + tr.findMin());
////        
//        Tree tr2 = new Tree();
//        System.out.println("Maximum value is " + tr2.findMax());
//        System.out.println("Minimum value is " + tr2.findMin());
//        System.out.println("1st tree size is " + tr.size());
//        System.out.println("2ns tree size is " + tr2.size());
//        
//        tr.remove(9);
//        tr.remove(88);
//        tr.remove(0);
//        tr.remove(-7);
//        tr.remove(67);
//        TreeTraversal.inorder(tr);
//        System.out.println("1st tree size is " + tr.size());
//        System.out.println("Maximum value is " + tr.findMax());
//        System.out.println("Minimum value is " + tr.findMin());
//        
//        tr2.remove(-5);
//        System.out.println("2ns tree size is " + tr2.size());
//        
//        System.out.println("Height of the tree is " + tr.height(tr.getRoot()));
    }
    
}
